from Robinhood import Robinhood
import numpy as np
import getpass 
my_trader = Robinhood()

try:
	username = input('Username: ')
	password = getpass.getpass('Password: ')
	logged_in = my_trader.login(username = username , password = password)
except:
	#note 2FA currently only works via sms mode
	twoFA = input("2FA Code: ") 
	logged_in = my_trader.login(username = username , password = password, mfa_code = twoFA)
	print('\nlogged in.')
	
def limBuy(ticker, low, high, invert = False):

	print('\n')


	diff = high - low

		
	#feature to invert the fib retracement levels, to be included later
	if invert == True:
		fibs = np.ones(5) - (.786, .618, .5, .382, .236)
		print('inverted mode')
	else:
		fibs = (.236, .382, .5, .618, .786)


	buys = []

	for ratio in fibs:
		price = round(high - ratio * diff, 2)
		buys.append(price)

	buys



	print('\n')

	for i in range(len(fibs)):
		print('level {} : ${:.2f}'.format(i+1, buys[i]))

	print('\n')

	quant = []

	for i in range(len(fibs)):
		quant.append(input('how many shares do you want to buy at level {} (${:.2f}):\t'.format(i, buys[i])))


	print('\n')
		
	for i in range(len(fibs)):
		print('# to buy at: ${:.2f} \t Quantity: {}'.format(buys[i],quant[i]))
		
	print('\n')	
		
	greenlit = input('Execute trades? (y/n):  ')

	if greenlit == 'y':
	
		for i in range(len(fibs)):
			
			if int(quant[i]) > 0:
				#print('buy at: {} \t Quantity: {}'.format(buys[i],quant[i]))
				buy_order = my_trader.place_limit_buy_order(symbol = ticker, 
                                                time_in_force = 'GFD', 
                                                price = buys[i], 
                                                quantity = quant[i])
			if i == len(fibs) - 1 : 
				print('\nTrades Executed.\n')
				
		
			
	elif greenlit == 'n':
		print('Order canceled.\n')
		
	else:
		print('Invalid input.\n')
		

	print('\n')
	

	
while True:
    print('\n')
    ticker = str.upper(input('input ticker symbol: '))
    invert = input('invert? (y/n) ')
    inverted = False
    
    if invert == 'y':
        print('Inverted')
        inverted = True
    elif invert == 'n':
        pass
    else:
        print('invalid input dummy, not inverting..')
    
    high = float(input('input HIGH: '))	
    low = float(input('input LOW: '))
    	
    limBuy(ticker, low, high, inverted)
    	
    newTrade = input('\nDo you want to submit another trade? (y/n)')
    	
    if newTrade != 'y':
        print('Logging Out...')
        print('Huzzah!')
        my_trader.logout()
        break
	
	
	
	
	


		


#p.tradesApproved()

# try:
# 	pf.plotCandle(ticker)
# except:
# 	print('failed to plot data')


